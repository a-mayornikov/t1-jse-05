package ru.t1.mayornikov.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextline() {
        return SCANNER.nextLine();
    }

}
